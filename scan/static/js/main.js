function systemcheck(){
	$.ajax({
		url : '/systemcheck',
		method : 'GET',
		dataType : 'json',
		success : function(data){
			if(data['result']){
				window.location.reload();
			}
		}
	});
}
$(document).ready(function(){
	interval = 5 * 1000;
	setInterval(systemcheck,interval);
});