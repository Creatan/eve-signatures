from flask import Flask, render_template, request, url_for,redirect, jsonify
from mongoengine import connect
from flask.ext.mongoengine import MongoEngine
import logging
from logging.handlers import RotatingFileHandler
import re
import redis

app = Flask(__name__)
app.config.from_object('config')

db = MongoEngine(app)

redis_server = redis.StrictRedis()

class Signature(db.Document):
	system = db.StringField(max_length=255, required=True)
	sig_id = db.StringField(max_length=255, required=True)
	sig_type = db.StringField(max_length=255, required=True)
	sig_name = db.StringField(max_length=255, required=True)


@app.route('/', methods=['GET','POST'])
def index():
	system = request.headers.get('Eve-solarsystemname')
	#get the existing signature identifiers
	signatures = Signature.objects(system=system)
	identifiers = []
	for signature in signatures:
		identifiers.append(signature.sig_id)

	if request.method == 'POST':
		rows = re.split("\n", request.form['systemdata'])
		for row in rows:
			data = re.split("\t",row)
			if len(data) >= 4:
				if data[0] not in identifiers:
					signature = Signature(
						system=system,
						sig_id=data[0],
						sig_type=data[2],
						sig_name=data[3]
					
					)
					signature.save()
				else:
					signature = signatures.get(sig_id=data[0])
					update = False
					if data[2] != '':
						signature.sig_type = data[2]
						update = True
					if data[3] != '':
						signature.sig_name = data[3]
						update = True
					if update:
						signature.save()
	
	if request.headers.get('Eve-trusted') == 'Yes' and system is not None:
		#find signatures in the system
		signatures = Signature.objects(system=system).order_by('sig_id')
		return render_template('index.html',system=system,signatures=signatures)
	else: 
		return render_template('trust.html')

@app.route('/clearall')
def clearall():
	Signature.drop_collection()
	return redirect(url_for('index'))

@app.route('/clear')
@app.route('/clear/<sig>')
def clear(sig=''):

	system = request.headers.get('Eve-solarsystemname')
	if sig:
		Signature.objects(sig_id=sig,system=system).delete()
	else:
		Signature.objects(system=system).delete()
	return redirect(url_for('index'))

@app.route('/systemcheck')
def systemcheck():
	user = request.headers.get('Eve-charid')
	key = 'last-system-{}'.format(user)
	result = False
	system = request.headers.get('Eve-solarsystemname')
	if(redis_server.get(key)):
		if(redis_server.get(key) != system):
			redis_server.set(key,system)
			result = True
		else:
			result = False
	else:
		redis_server.set(key,system)
	return jsonify(result=result)

@app.route('/help')
def help():
	return render_template('help.html')